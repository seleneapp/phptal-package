<?php

/**
 * This File is part of the Selene\Packages\Framework\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\PhpTal\Process;

use \Selene\Components\DI\Reference;
use \Selene\Components\DI\Definition\FlagInterface;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Processor\ProcessInterface;

/**
 * @class SubscribeMiddlewares
 * @package Selene\Packages\Framework\Process
 * @version $Id$
 */
class RegisterEngine implements ProcessInterface
{
    /**
     * process
     *
     * @param ContainerInterface $container
     *
     * @access public
     * @return mixed
     */
    public function process(ContainerInterface $container)
    {
        $this->container = $container;

        if (!$view = $container->getDefinition('view')) {
            return;
        }

        $engines = (array)$view->getArgument(0);
        $engines[] = new Reference('phptal.engine');

        $view->replaceArgument($engines, 0);
    }
}
