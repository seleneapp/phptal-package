<?php

/**
 * This File is part of the Selene\Packages\PhpTal package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\PhpTal\Process;

use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Processor\ProcessInterface;

/**
 * @class EnsureCacheLocation
 * @package Selene\Packages\PhpTal
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class EnsureCacheLocation implements ProcessInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerInterface $container)
    {
        // ensure cache directory exists
        $parameters = $container->getParameters();

        if ($cacheDir = $parameters->get('phptal.template_cache')) {
            if (!is_dir($cacheDir)) {
                @mkdir($cacheDir, 0777, true);
            }
        }
    }
}
