<?php

/**
 * This File is part of the Selene\Packages\PhpTal package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\PhpTal;

use \Selene\Components\Package\Package;
use \Selene\Components\DI\BuilderInterface;
use \Selene\Components\DI\Processor\ProcessorInterface;
use \Selene\Packages\PhpTal\Process\RegisterEngine;
use \Selene\Packages\PhpTal\Process\EnsureCacheLocation;

/**
 * @class PhpTalPackage extends Package PhpTalPackage
 * @see Package
 *
 * @package Selene\Packages\PhpTal
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class PhpTalPackage extends Package
{
    public function build(BuilderInterface $builder)
    {
        $builder->getProcessor()
            ->add(new RegisterEngine, ProcessorInterface::OPTIMIZE)
            ->add(new EnsureCacheLocation, ProcessorInterface::AFTER_REMOVE);
    }
}
