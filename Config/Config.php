<?php

/**
 * This File is part of the Selene\Packages\PhpTal package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\PhpTal\Config;

use \Selene\Components\DI\BuilderInterface;
use \Selene\Components\Package\PackageConfiguration;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Loader\XmlLoader;
use \Selene\Components\Config\Resource\Locator;

/**
 * @class Config
 * @package Selene\Packages\PhpTal
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Config extends PackageConfiguration
{
    /**
     * {@inheritdoc}
     */
    public function load(BuilderInterface $builder, array $values)
    {
        $container = $builder->getContainer();

        $loader = $this->getConfigLoader($builder, $locator = new Locator([$this->getResourcePath()]));

        $loader->load('services.xml');

        $config = $this->mergeValues($values);

        $container->setParameter(
            'phptal.output_mode',
            $this->getOutputMode(
                $this->getDefault($config, 'output_mode', $container->getParameter('phptal.output_mode'))
            )
        );

        $container->setParameter(
            'phptal.template_cache',
            $this->getDefault($config, 'template_cache', $container->getParameter('phptal.template_cache'))
        );
    }

    /**
     * getOutputMode
     *
     * @param mixed $mode
     *
     * @return int
     */
    private function getOutputMode($mode)
    {
        if (is_int($mode)) {
            return $mode;
        }

        switch (strtolower($mode)) {
            case 'xhtml':
                return \PHPTAL::XHTML;
            case 'html5':
                return \PHPTAL::HTML5;
            case 'xml':
                return \PHPTAL::XML;
        }
    }
}
