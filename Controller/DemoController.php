<?php

/**
 * This File is part of the Selene\Packages\PhpTal package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\PhpTal\Controller;

use \Selene\Components\Routing\Controller\Controller;

/**
 * @class DemoController
 * @package Selene\Packages\PhpTal
 * @version $Id$
 */
class DemoController extends Controller
{
    public function indexAction()
    {
        return $this->render('php_tal::demo.xhtml');
    }
}
